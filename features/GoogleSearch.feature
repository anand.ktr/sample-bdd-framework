#Sample Feature Definition Template for google india search
Feature: Verify the google search working as expected or not

Background: Navigates to google india portal
    Given the user is navigates to google india portal

  @google
  Scenario Outline: Verify the google search working as expected or not
    When the user searches the <inputText> value in google india
    Then the user verifies that the search result page title contains the <inputText>

    Examples: 
      | inputText      |
      | selenium       |

  @google
  Scenario Outline: Verify the google search working as expected or not
    When the user searches the <inputText> value in google india
    Then the user verifies that the search result page title contains the <inputText>

    Examples: 
      | inputText      |
      | java           |
