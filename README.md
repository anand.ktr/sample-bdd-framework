**Overview of the Automation framework (BDD + TestNG + Selenium + Cucumber + Java):**


We are following the Behavior-driven development (BDD) approach for automation. The actions are described in plain english language using 
"given", "when" and "then".
Each scenario will be saved as a feature file where any non-technical person also can understand what we are testing, which a big advantage of BDD as it can reach a wider audience.

Cucumber is a software tool that supports behavior-driven development. Cucumber decodes these Gherkin lines and executes the under hood code written in JAVA to perform actions on the web browsers leveraging Selenium web driver capabilities.

**Tech Stack:**

•	Coding language – Java
•	Build Management - Maven
•	IDE – Eclipse 
•	Selenium WebDriver - Automation tool
•	Build Management – Maven
•	Framework – TestNG + Cucumber
•	Design Pattern – POM (Page Object Model)
•	Reporting – Log4J


**Installation:**
1. Download Java jdk from https://www.oracle.com/in/java/technologies/downloads/ and install the same
2. Set up the JAVA_HOME variable under system variables by providing the path where the java jdk folder is located
3. Under system variables, click on Path and edit
4. Provide the jdk folder bin
5. Provide the jre folder bin
6. Save the environment variable changes made
7. Download and install Eclipse IDE from https://www.eclipse.org/downloads/packages/release/helios/sr1/eclipse-ide-java-developers
8. Install GIT and create a folder for the project
9. Set up the SSH keys which is required for the host to identify your local machine by following https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent
10. Clone the repository locally to the newly created directory
11. Open the project using Eclipse IDE and build
12. From Eclipse marketplace (Help > Marketplace) install Cucumber and TestNG for IDE
13. Selenium, TestNG and Cucumber installations would be automatically taken care by Maven from the dependency file


**How to run the selected feature:**
1. Open command prompt
2. CD to the deviceregistration-automation
3. Choose the test to run from the Features folder
4. Note down the tag mentioned in the feature
5. Use the command "mvn test -Dcucumber.filter.tags="@google" -Dbrowser=edge" to run the google feature (replace the tag with the tag noted down in step 4 above)
6. Change the -Dbrowser to firefox, chrome or edge to run the test in respective browsers

**Report generation:**
After the test execution, the report would be generated in the path that is metioned in the Cucumber options -> TestNGRunner. Currently the path given is "./target/cucumber/cucumber.html".

