package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import utils.GenericUtils;
import utils.TestBase;


public class GoogleHomePO {
	public WebDriver driver;
	GenericUtils genericUtils;
	TestBase testBase;

	public GoogleHomePO(WebDriver driver) {
		this.driver = driver;
		this.genericUtils = new GenericUtils(driver);
		this.testBase = new TestBase();
	}

	By txtSearchArea = By.xpath("//textarea[@name='q']");
	By btnSearch = By.name("btnK");
	

	/**
	 * Method used to navigates to google india portal
	 */
	public void navigatesToGoogleIndia(){

		this.driver.get(genericUtils.getConfigPropertyValue("googleIndiaUrl"));
		Assert.assertTrue(genericUtils.waitforWebElement(txtSearchArea),
		"Google search area is not available");
		testBase.logger.info("Google home page is displayed successfully.");
	}

	/**
	 * Method used to search given values in google
	 * 
	 * @param inputText - User input to search
	 */
	public void googleSearch(String inputText){

		genericUtils.sendkeysToWebElement(txtSearchArea, inputText);
		genericUtils.click(btnSearch);
		testBase.logger.info("Successfully searched from google with " + inputText);
	}

	/**
	 * Method used to verify the google search result title
	 * 
	 * @param inputText - Given text should be the part of title
	 */
	public void verifyGoogleTitle(String inputText){
		
		String actualTitle = this.driver.getTitle();
		Assert.assertTrue(actualTitle.contains(inputText), "Google page title is not expected one.");
		testBase.logger.info("Google page title is displayed as expected");
	}

}
