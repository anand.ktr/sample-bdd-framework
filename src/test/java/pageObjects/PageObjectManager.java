package pageobjects;

import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class PageObjectManager {

	public GoogleHomePO googleHomePO;
	public WebDriver driver;

	public PageObjectManager(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * Method will return google home page object
	 * 
	 * @return google home page object
	 */
	public GoogleHomePO getGoogleHomePO() {
		googleHomePO = new GoogleHomePO(driver);
		return googleHomePO;
	}

}
