package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import pageobjects.GoogleHomePO;
import utils.TestContextSetup;

public class GoogleHomeSteps {
	public WebDriver driver;
	TestContextSetup testContextSetup;
	GoogleHomePO googleHomePO;

	public GoogleHomeSteps(TestContextSetup testContextSetup) throws IOException {
		this.testContextSetup = testContextSetup;
		this.googleHomePO = testContextSetup.pageObjectManager.getGoogleHomePO();
	}

	/**
	 * Step used to navigate to google india site
	 */
	@Given("^the user is navigates to google india portal$")
	public void the_user_is_navigates_to_google_india_portal() {
		
		googleHomePO.navigatesToGoogleIndia();
	}

	/**
	 * Step used to search the input value into goole search
	 */
	@When("^the user searches the <inputText> value in google india$")
	public void the_user_searches_input_value(String inputText) throws IOException {

		googleHomePO.googleSearch(inputText);
	}

	/**
	 * Step used to verify the google title
	 */
	@Then("^the user verifies that the search result page title contains the <inputText>$")
	public void the_user_verifies_page_title(String inputText) throws Exception {

		googleHomePO.verifyGoogleTitle(inputText);
	}
}
