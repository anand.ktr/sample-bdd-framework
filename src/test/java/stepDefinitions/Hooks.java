package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.monte.media.Format;
import org.monte.media.FormatKeys;
import org.monte.media.math.Rational;

import static org.monte.media.AudioFormatKeys.*;
import static org.monte.media.VideoFormatKeys.*;
import org.testng.SkipException;

import utils.TestContextSetup;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

public class Hooks {
	TestContextSetup testContextSetup;
	static boolean runFlag = true;
	static String screenRecordingFlag;
	private static ScreenRecorder screenRecorder;

	public Hooks(TestContextSetup testContextSetup) {
		this.testContextSetup = testContextSetup;
	}

	@Before
	public void beforeScenario(Scenario scenario) throws IOException, AWTException {
		if (runFlag) {
			testContextSetup.testBase.logger.info(scenario.getName() + " - Scenario started to execute.");
			// Screen recording block
			screenRecordingFlag = System.getProperty("screenRecording");
			if(screenRecordingFlag.equalsIgnoreCase("ON")) {
				testContextSetup.testBase.logger.info("*********** Screen recording is ON ***************");
				String currentFilePath = scenario.getUri().toString();
				String folderName = currentFilePath.substring(currentFilePath.lastIndexOf("/") + 1).replaceAll(".feature",
						"");
				startScreenRecording(folderName);
			} else {
				testContextSetup.testBase.logger.info("*********** Screen recording is OFF ***************");
			}
			
		} else {
			testContextSetup.testBase.logger.info("Previous scenario got failed. Hence skipping the execution.");
			throw new SkipException("*********** Skipping the execution ***************");
		}
	}

	@After
	public void AfterScenario(Scenario scenario) throws IOException {
		String impTimeValue = testContextSetup.testBase.getPropertyValue("logoutWaitTime");
		int waitTime = Integer.parseInt(impTimeValue);
		testContextSetup.testBase.driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(waitTime));
		testContextSetup.testBase.setup().quit();
		testContextSetup.testBase.logger.info("************ Quitting the BROWSER ************");
		if (scenario.isFailed()) {
			runFlag = false;
			testContextSetup.testBase.logger.info(scenario.getName() + " - Scenario got failed.");
		}
		
		if(screenRecordingFlag.equalsIgnoreCase("ON")) {
			stopScreenRecording();
		}
	}

	@AfterStep
	public void AddScreenshot(Scenario scenario) throws IOException {
		WebDriver driver = testContextSetup.testBase.setup();
		if (scenario.isFailed()) {
			// screenshot
			File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			byte[] fileContent = FileUtils.readFileToByteArray(sourcePath);
			scenario.attach(fileContent, "image/png", "image");
			String screenshotName = scenario.getName().replaceAll(" ", "_")
					+ testContextSetup.genericUtils.getCurrentDatewithTime("ddHHmmss");
			File destinationPath = new File(
					System.getProperty("user.dir") + "\\target\\screenshots\\" + screenshotName + ".png");
			FileUtils.copyFile(sourcePath, destinationPath);
		}

	}
	
	/**
	 * Method used to start the screen recording
	 * 
	 * @param folderName - Folder name to save recorded video
	 * @throws IOException
	 * @throws AWTException
	 */
	public void startScreenRecording(String folderName) throws IOException, AWTException {
		File file = new File(System.getProperty("user.dir") + "\\target\\video-recording\\" + folderName);
		// Create a instance of GraphicsConfiguration to get the Graphics configuration
		// of the Screen. This is needed for ScreenRecorder class.
		GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
				.getDefaultConfiguration();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = screenSize.width;
		int height = screenSize.height;

		Rectangle captureSize = new Rectangle(0, 0, width, height);
		Format videoFileFormat = new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI);
		Format screenFormat = new Format(MediaTypeKey, FormatKeys.MediaType.VIDEO, EncodingKey,
				ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
				DepthKey, (int) 24, FrameRateKey, Rational.valueOf(15), QualityKey, 1.0f, KeyFrameIntervalKey,
				(int) (15 * 60));
		Format mouseFormat = new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, "black", FrameRateKey,
				Rational.valueOf(30));

		// Create a instance of ScreenRecorder with the required configurations
		screenRecorder = new ScreenRecorder(gc, captureSize, videoFileFormat, screenFormat, mouseFormat, null,
				file);

		screenRecorder.start();
	}
	
	/**
	 * Method used to stop the screen recording and save
	 * @throws IOException
	 */
	public void stopScreenRecording() throws IOException {
		screenRecorder.stop();

		List<File> createdMovieFiles = screenRecorder.getCreatedMovieFiles();
		for (File movie : createdMovieFiles) {
			testContextSetup.testBase.logger.info("New movie created: " + movie.getAbsolutePath());
			testContextSetup.testBase.logger.info("*********** Screen recording stopped ***************");
		}
	}
}
