package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.Properties;

public class TestBase {

	public WebDriver driver;
	WebDriverWait wait;
	public static String browserType;
	public Logger logger = LogManager.getLogger(TestBase.class);

	public WebDriver setup() throws IOException {

		browserType = System.getProperty("browser"); // These properties would be
		// picked from the mvn test command from the
		// sample mvn command would look: mvn test -Dcucumber.filter.tags="@googleTest"
		// -Dbrowser=edge
		if (driver == null) {
			try {
				if (browserType.toLowerCase().contains("chrome")) {
					WebDriverManager.chromedriver().setup();
					driver = new ChromeDriver();

				} else if (browserType.toLowerCase().contains("edge")) {
					WebDriverManager.edgedriver().setup();
					driver = new EdgeDriver();
				}
			} catch (NullPointerException NPE) {
				logger.error("The provided value for browser is: " + browserType
						+ ", which is invalid. Please provide proper value from command prompt during the run.");
			}
			driver.manage().timeouts()
					.implicitlyWait(Duration.ofSeconds(Integer.parseInt(getPropertyValue("implicitDriverWaitTime"))));
			driver.manage().window().maximize();
			wait = new WebDriverWait(driver,
					Duration.ofSeconds(Integer.parseInt(getPropertyValue("explicitDriverWaitTime"))));
			logger.info("************ INITIALIZED BROWSER ************" + driver);

		}
		return driver;

	}

	public String getPropertyValue(String key) throws IOException {
		FileInputStream fis = new FileInputStream(
				System.getProperty("user.dir") + "//src//test//resources//config.properties");
		Properties prop = new Properties();
		prop.load(fis);
		return prop.getProperty(key);
	}
}
