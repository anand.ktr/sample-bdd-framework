package utils;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class GenericUtils {
	public WebDriver driver;
	public WebDriverWait wait;
	TestBase testBase;

	public GenericUtils(WebDriver driver) throws NumberFormatException, IOException {
		this.driver = driver;
		this.testBase = new TestBase();
	}

	// Find element method - will return a single webelement
	public WebElement getElement(By locator) {
		WebElement element = null;
		try {
			testBase.logger.info("Waiting for the element. " + locator.toString());
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			element = driver.findElement(locator);
			testBase.logger.debug("Found and return the element. " + element.getAccessibleName());
			return element;
		} catch (Exception e) {
			testBase.logger.error("Unable to finding an element: ");
			e.printStackTrace();
		}
		return null;
	}

	// Find elements method - will return a list of webelement
	public List<WebElement> getElements(By locator) {
		List<WebElement> elements = null;
		try {
			testBase.logger.info("Waiting for all elements. " + locator.toString());
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			elements = driver.findElements(locator);
			testBase.logger
					.debug("Found and return the list of elements. List contains " + elements.size() + " elements");
			return elements;
		} catch (Exception e) {
			testBase.logger.error("Unable to finding list of element : ");
			e.printStackTrace();
		}
		return null;
	}

	// Send the value to given input field
	public void sendkeysToWebElement(By locator, String keysToSend) {
		WebElement element = null;
		try {
			element = getElement(locator);
			element.clear();
			testBase.logger.info("Clearing the element. " + locator.toString());
			testBase.logger.debug("Sent the given text to the element. " + element.getAccessibleName());
			element.sendKeys(keysToSend);
		} catch (Exception e) {
			testBase.logger.error("Unable to send the text to element : ");
			e.printStackTrace();
		}
	}

	// This method return the element's string value
	public String getElementText(By locator) {
		WebElement element = null;
		try {
			element = getElement(locator);
			return element.getText();
		} catch (Exception e) {
			testBase.logger.error("Unable to get the text from element : ");
			e.printStackTrace();
			return null;
		}
	}

	// Normal Click method - wait and click the element
	public boolean click(By locator) {
		WebElement elementToBeClicked = null;
		try {
			elementToBeClicked = getElement(locator);
			testBase.logger.info("Waiting for the clickable element. " + elementToBeClicked.toString());
			wait.until(ExpectedConditions.elementToBeClickable(elementToBeClicked));
			testBase.logger.debug("Element has been clicked. " + elementToBeClicked.getAccessibleName());
			// Thread.sleep(2000);
			elementToBeClicked.click();
			return true;
		} catch (Exception e) {
			testBase.logger.error("Unable to click an element : ");
			return false;
		}
	}


	// wait until expected text to be element
	public void waitForWebElementText(By locator, String text) {
		try {
			wait.until(ExpectedConditions.textToBe(locator, text));
		} catch (Exception e) {
			testBase.logger.error("Unable to find the element : ");
			System.out.println("Unable to find the element : ");
		}
	}

	/**
	 * getConfigPropertyValue - It will return appropriate config value from
	 * property file
	 *
	 * @param key - Denotes property key
	 * @return String - Appropriate value assign for the give key
	 * @throws IOException
	 */
	public String getConfigPropertyValue(String key) throws IOException {
		FileInputStream fis = new FileInputStream(
				System.getProperty("user.dir") + "//src//test//resources//config.properties");
		Properties prop = new Properties();
		prop.load(fis);
		return prop.getProperty(key);
	}

	
	public boolean waitforWebElement(By locator) {
		boolean status = wait.until(ExpectedConditions.presenceOfElementLocated(locator)) != null;
		return status;

	}

	// This method will return current(today) date as string
	public String get_current_date() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}
}
